import java.util.Scanner;


public class LuckyCardGameApp {
    public static void main(String[] args) {
        java.util.Scanner reader = new java.util.Scanner(System.in);
		
        Deck newDeck = new Deck();
        newDeck.shuffle();

        System.out.print("How many cards will you draw? > ");
        int drawing = reader.nextInt();

        System.out.println("Currently " + newDeck.length() + " cards");

        for (int i = 0; i < drawing; i++){
            System.out.println(newDeck.drawTopCard().toString());
        }

        System.out.println("Currently " + newDeck.length() + " cards");

        newDeck.shuffle();

        System.out.println(newDeck.toString());
    }


}