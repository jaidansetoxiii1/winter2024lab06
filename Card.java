import java.util.Scanner;
/*
* skeleton code to do your lab work
* change the class name and save with that file name
* delete these 3 lines and add a description
* put your name after author:
*
* author: put your name here
*/ 

public class Card {

	//fields
	private String suit;
	private int value;
	
	//Constructor Method
	public Card (String iSuit, int iValue){
		this.suit = iSuit;
		this.value = iValue;
	}
	
	//Getter Methods
	public String getSuit(){
		return this.suit;
	}
	
	public int getValue(){
		return this.value;
	}
	
	//Setter Methods
	public void setSuit(String newSuit){
		this.suit = newSuit;
	}
	
	public void setValue(int newValue){
		this.value = newValue;
	}

	//toString Method
	public String toString() {
		return (this.value + " of " + this.suit + "s");
	}
}